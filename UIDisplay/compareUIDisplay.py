#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Time : 2022/5/31 22:10
# @Author : rynez
# @File : compareUIDisplay.py
# @Purpose : 输出展示页面，提供新旧对比

from PyQt5.QtWidgets import *
from PyQt5 import QtCore, QtGui
from UIDisplay.compareFormUI import *
import logging


class resultCompareWindow(QWidget,Ui_Form3):
    def __init__(self):
        super(resultCompareWindow, self).__init__()

        self.setupUi(self)
        self.oldConfig.setLineWrapMode(0)
        self.matchdContent.setLineWrapMode(0)
        self.newContent.setLineWrapMode(0)
        self.outputcomlist=[]
        self.saveButton.clicked.connect(self.saveButton_clicked)
        self.prescrollbar=self.oldConfig.verticalScrollBar()
        self.matchedscrollbar=self.matchdContent.verticalScrollBar()
        self.newscrollbar=self.newContent.verticalScrollBar()
        self.showlinebar=self.showline.verticalScrollBar()
        self.prescrollbar.valueChanged.connect(self.prescrollbar_changed)
        self.matchedscrollbar.valueChanged.connect(self.matchedscrollbar_changed)
        self.newscrollbar.valueChanged.connect(self.newscrollbar_changed)


    def open(self,precommands,matchedcommands,results):
        self.show()
        #self.setFixedSize(self.width(), self.height())
        # print(precommands)
        #print(results)
        precommandoutput=[]
        matchcomoutput=[]
        resultoutput=[]
        totalcomnum=0
        for count,values in precommands.items():
            perblockcomnum=len(precommands[count])
            resultcomnum=len(results[count])
            #print(results[count])
            matchedcomlist=[]
            for modulenum,commands in matchedcommands[count].items():
                for line in commands:
                    matchedcomlist.append(line)
            #print(matchedcommands[count])
            matchedcomnum=len(matchedcomlist)
            #获取这三个中最大的行数
            maxcomnum = (perblockcomnum if perblockcomnum > resultcomnum else resultcomnum) if (perblockcomnum if perblockcomnum > resultcomnum else resultcomnum) > matchedcomnum else matchedcomnum
            #print(perblockcomnum,matchedcomnum,resultcomnum,maxcomnum)
            precommandoutput.append('\n'.join(precommands[count]+['' for i in range(maxcomnum-perblockcomnum)]))
            matchcomoutput.append('\n'.join(matchedcomlist+['' for i in range(maxcomnum - matchedcomnum)]))
            resultoutput.append('\n'.join(results[count]+['' for i in range(maxcomnum - resultcomnum)]))
            self.outputcomlist.extend(results[count])
            totalcomnum=totalcomnum+maxcomnum

        linenumlist=[str(i) for i in range(totalcomnum)]
        self.showline.setMaximumWidth(len(str(totalcomnum))*10+10)
        self.oldConfig.setText('\n'.join(precommandoutput))
        self.matchdContent.setText('\n'.join(matchcomoutput))
        self.newContent.setText('\n'.join(resultoutput))
        self.showline.setText('\n'.join(linenumlist))
        self.newContent.verticalScrollBar().setValue(10)

    def saveButton_clicked(self):
        filename, _ = QFileDialog.getSaveFileName(
            self, '保存文件', '.', '所有文件(*.txt)')
        try:

            outputstr = '\n'.join(self.outputcomlist)
            with open(filename, 'w') as f:
                f.write(outputstr)
        except Exception as E:
            logging.critical('unknown error:' + str(E))
            return 0



    def prescrollbar_changed(self):
        self.matchedscrollbar.setValue(self.prescrollbar.value())
        self.newscrollbar.setValue(self.prescrollbar.value())
        self.showlinebar.setValue(self.prescrollbar.value())

    def matchedscrollbar_changed(self):
        self.prescrollbar.setValue(self.matchedscrollbar.value())
        self.newscrollbar.setValue(self.matchedscrollbar.value())
        self.showlinebar.setValue(self.matchedscrollbar.value())

    def newscrollbar_changed(self):
        self.prescrollbar.setValue(self.newscrollbar.value())
        self.matchedscrollbar.setValue(self.newscrollbar.value())
        self.showlinebar.setValue(self.newscrollbar.value())