#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Time : 2022/5/31 22:15
# @Author : rynez
# @File : tamplateguideUIDisplay.py
# @Purpose : 初始模板向导页面

import os
import json
import shlex
import logging

from PyQt5.QtWidgets import *
from PyQt5 import QtCore
from PyQt5.QtGui import *
from UIDisplay.tamplateguideFormUI import *

class tampSetWindow(QWidget, Ui_Form2):
    def __init__(self):
        super(tampSetWindow, self).__init__()
        self.setupUi(self)
        self.commandlist = []  # 存放分解后的命令
        self.checkboxchecked = []  # 存放哪些位置被选中了
        self.isconfiged = []  # 存放哪些行已经设置参数了
        self.moduleDict = {
            'startwith': '',
            'perline': {},
            'endwith': ''}  # 根据此字典生成最终内容
        self.dealcommandButton.clicked.connect(self.dealcommandButton_clicked)
        # self.dealtableWidget.cellPressed.connect(self.getPosContent)
        self.setMultiStartButton.clicked.connect(
            self.setMultiStartButton_clicked)
        self.setMultiEndButton.clicked.connect(self.setMultiEndButton_clicked)
        self.setsingleStartButton.clicked.connect(
            self.setsingleStartButton_clicked)
        self.setParaButton.clicked.connect(self.setParaButton_clicked)
        self.clearButton.clicked.connect(self.clearButton_clicked)
        self.saveFileButton.clicked.connect(self.saveFileButton_clicked)

    def open(self):
        self.show()
        self.setFixedSize(self.width(), self.height())

##槽begin##
    def dealcommandButton_clicked(self):
        # 每次点击会初始化各列表和字典
        self.moduleDict = {'startwith': '', 'perline': {}, 'endwith': ''}
        self.commandlist = []
        self.checkboxchecked = []
        self.isconfiged = []
        lencommand = 0
        commandsstr = self.commandsEdit.toPlainText()
        commands = commandsstr.split('\n')
        for line in commands:
            linetemp = shlex.shlex(line, punctuation_chars=True)
            # linetemp.whitespace=' '
            linetemp.commenters = ''
            #linetemp.quotes = ''
            linetemp.whitespace_split = True
            commandsplit = list(linetemp)
            #commandsplit = line.split()
            self.commandlist.append(commandsplit)
            if len(commandsplit) > lencommand:
                lencommand = len(commandsplit)
        self.commandlist = list(filter(None, self.commandlist))
        # self.model=QStandardItemModel(len(commands),len(commandlist))
        # #self.model.setHorizontalHeaderLabels(['a','b','c'])
        # #将QTableView(创建的表格控件)与model(模型)关联
        # self.dealtableView.setModel(self.model)
        self.dealtableWidget.setRowCount(len(self.commandlist))
        self.dealtableWidget.setColumnCount(lencommand)
        # self.dealtableWidget.resizeColumnsToContents()
        self.dealtableWidget.resizeRowsToContents()
        # 展示数据
        for i in range(len(self.commandlist)):
            position = 0
            # 继续初始化字典moduleDict
            self.moduleDict['perline'][i] = {}
            for percommand in self.commandlist[i]:
                # print(percommand)
                # 初始化表格，添加复选框，默认未选择状态
                commanditem = QTableWidgetItem(percommand)
                commanditem.setTextAlignment(QtCore.Qt.AlignRight)
                self.dealtableWidget.setItem(i, position, commanditem)
                checkbox = QtWidgets.QCheckBox()
                checkbox.setCheckState(QtCore.Qt.Unchecked)
                # checkbox.setText(percommand)
                checkbox.setStyleSheet('QCheckBox{margin:3px}')
                #checkbox.stateChanged.connect(lambda state: print(state))
                # 当复选框状态改变时，在变量中添加或移除当前复选框的位置
                checkbox.stateChanged.connect(
                    lambda state: self.checkboxState_changed(state))
                self.horizontalLayout.addWidget(checkbox)
                self.dealtableWidget.setCellWidget(i, position, checkbox)

                position = position + 1
        # self.modulestrEdit.setText(commandsstr)

    # def getPosContent(self,row,col):
    #     try:
    #         content = self.dealtableWidget.item(row, col).text()
    #         print('中文测试')
    #         print("选中行" + str(row))
    #         print("选中列" + str(col))
    #         print("选中内" + content)
    #     except:
    #         print("选中内容为空")

    def checkboxState_changed(self, curstate):
        curcloumn = self.dealtableWidget.currentColumn()
        currow = self.dealtableWidget.currentRow()
        if curstate == 2:
            self.checkboxchecked.append([currow, curcloumn])
        elif curstate == 0:
            self.checkboxchecked.remove([currow, curcloumn])
        self.checkboxchecked.sort()
        print(self.checkboxchecked)

    def setMultiStartButton_clicked(self):

        if not self.dealtableWidget.item(0, 0):
            self.showDialog('warning', '请先填入需要转换的命令并点击预处理按钮!')
        elif not self.checkboxchecked:
            self.showDialog('warning', '请先勾选出单行或者多行命令的特征字符串!')
        elif self.checkboxchecked[0][0]:
            self.showDialog('warning', '当前选择的不是首行，请重新选择内容！')
        else:
            self.moduleDict['startwith'] = '.*'
            lastnum = self.checkboxchecked[0][1] - 1
            firstsign = 1
            jundgeissameline = self.checkboxchecked[0][0]
            for parm in self.checkboxchecked:
                if jundgeissameline == parm[0]:
                    cellcontent = self.dealtableWidget.item(
                        parm[0], parm[1]).text()
                    # 如果连续的话直接添加\s
                    if lastnum + 1 == parm[1]:
                        if firstsign:
                            self.moduleDict['startwith'] = self.moduleDict['startwith'] + cellcontent
                            firstsign = 0
                        else:
                            self.moduleDict['startwith'] = self.moduleDict['startwith'] + \
                                '\\s' + cellcontent

                    # 如果不连续则多添加.*匹配
                    else:
                        # if addany>1:
                        self.moduleDict['startwith'] = self.moduleDict['startwith'] + \
                            '.*\\s' + cellcontent

                        # addany=addany+1
                else:
                    self.showDialog('warning', '勾选的内容不在同一行，请检查后再进行下一步')
                    self.moduleDict['startwith'] = ''
                    break
                lastnum = parm[1]
            self.printModule()
            # self.modulestrEdit.setText(''.join(self.checkboxchecked))

    def setMultiEndButton_clicked(self):
        if not self.dealtableWidget.item(0, 0):
            self.showDialog('warning', '请先填入需要转换的命令并点击预处理按钮!')
        elif not self.checkboxchecked:
            self.showDialog('warning', '请先勾选出单行或者多行命令的特征字符串!')
        else:
            self.moduleDict['endwith'] = '.*'
            lastnum = self.checkboxchecked[0][1] - 1
            firstsign = 1
            jundgeissameline = self.checkboxchecked[0][0]
            for parm in self.checkboxchecked:
                if jundgeissameline == parm[0]:
                    cellcontent = self.dealtableWidget.item(
                        parm[0], parm[1]).text()
                    # 如果连续的话直接添加\s
                    if lastnum + 1 == parm[1]:
                        if firstsign:
                            self.moduleDict['endwith'] = self.moduleDict['endwith'] + cellcontent
                            firstsign = 0
                        else:
                            self.moduleDict['endwith'] = self.moduleDict['endwith'] + \
                                '\\s' + cellcontent

                    # 如果不连续则多添加.*匹配
                    else:
                        # if addany>1:
                        self.moduleDict['endwith'] = self.moduleDict['endwith'] + \
                            '.*\\s' + cellcontent
                else:
                    self.showDialog('warning', '勾选的内容不在同一行，请检查后再进行下一步')
                    self.moduleDict['endwith'] = ''
                    break
                lastnum = parm[1]
            self.printModule()

    def setsingleStartButton_clicked(self):
        # 'perline':{}
        if not self.dealtableWidget.item(0, 0):
            self.showDialog('warning', '请先填入需要转换的命令并点击预处理按钮!')
        elif not self.checkboxchecked:
            self.showDialog('warning', '请先勾选出单行或者多行命令的特征字符串!')
        else:
            # self.checkboxchecked[0][0]为当前行数，[0][1]为列数
            self.moduleDict['perline'][self.checkboxchecked[0]
                                       [0]]['startwith'] = '.*'
            lastnum = self.checkboxchecked[0][1] - 1
            firstsign = 1
            jundgeissameline = self.checkboxchecked[0][0]
            for parm in self.checkboxchecked:
                if jundgeissameline == parm[0]:
                    cellcontent = self.dealtableWidget.item(
                        parm[0], parm[1]).text()
                    # 如果连续的话直接添加\s
                    if lastnum + 1 == parm[1]:
                        if firstsign:
                            self.moduleDict['perline'][self.checkboxchecked[0][0]
                                                       ]['startwith'] = self.moduleDict['perline'][self.checkboxchecked[0][0]]['startwith'] + cellcontent
                            firstsign = 0
                        else:
                            self.moduleDict['perline'][self.checkboxchecked[0][0]]['startwith'] = self.moduleDict[
                                'perline'][self.checkboxchecked[0][0]]['startwith'] + '\\s' + cellcontent

                    # 如果不连续则多添加.*匹配
                    else:
                        # if addany>1:
                        self.moduleDict['perline'][self.checkboxchecked[0][0]]['startwith'] = self.moduleDict[
                            'perline'][self.checkboxchecked[0][0]]['startwith'] + '.*\\s' + cellcontent
                else:
                    self.showDialog('warning', '勾选的内容不在同一行，请检查后再进行下一步')
                    self.moduleDict['perline'][self.checkboxchecked[0]
                                               [0]]['startwith'] = ''
                    break
                lastnum = parm[1]
        self.printModule()

    def setParaButton_clicked(self):
        parasign = self.paraSignEdit.text()
        position = self.positionEdit.text()
        currow = self.checkboxchecked[0][0]  # 当前行数
        curcolumn = self.checkboxchecked[0][1]
        # if not self.isconfiged:
        #     #存放携带参数信息的
        currowcommandlist = self.commandlist[currow]  # 当前行命令的列表
        if not self.dealtableWidget.item(0, 0):
            self.showDialog('warning', '请先填入需要转换的命令并点击预处理按钮!')
            logging.warning('未输入内容')
        elif not self.checkboxchecked:
            self.showDialog('warning', '请先勾选一个参数!')
        elif len(self.checkboxchecked) > 1:
            self.showDialog('warning', '请不要勾选多个!')
        elif not parasign or not position:
            self.showDialog('warning', '请先按要求输入对应内容!')
        else:
            # self.checkboxchecked[0][0]为当前行数，[0][1]为列数
            if currow not in self.isconfiged:
                newcommandlist = []
                for i in range(len(currowcommandlist)):
                    newcommandlist.append([])
                    newcommandlist[i].append({currowcommandlist[i]: []})
                    self.isconfiged.append(currow)
            else:
                newcommandlist = self.moduleDict['perline'][currow]['command']
        # print(newcommandlist[curcolumn])
        # print(currowcommandlist[curcolumn])
        try:
            newcommandlist[curcolumn][0][currowcommandlist[curcolumn]].append(parasign + ':' + position)
        except UnboundLocalError:

            return 0
        self.moduleDict['perline'][currow]['command'] = newcommandlist
        self.printModule()

    def clearButton_clicked(self):
        currow = self.checkboxchecked[0][0]
        curcolumn = self.checkboxchecked[0][1]
        currowcommandlist = self.commandlist[currow]
        if not self.dealtableWidget.item(0, 0):
            self.showDialog('warning', '请先填入需要转换的命令并点击预处理按钮!')
        elif not self.checkboxchecked:
            self.showDialog('warning', '请先勾选一个参数!')
        elif len(self.checkboxchecked) > 1:
            self.showDialog('warning', '请不要勾选多个!')
        else:
            try:
                self.moduleDict['perline'][currow]['command'][curcolumn][0][currowcommandlist[curcolumn]] = [
                ]
                self.printModule()
            except KeyError:
                self.showDialog('warning', '选中内容未设置为参数，请检查后继续!')

    def printModule(self):
        # 处理多行开始标志
        print(self.moduleDict)
        multistart = ''
        commandsstr = ''
        multiend = ''
        if self.moduleDict['startwith']:
            self.moduleDict['startwith']=self.dealBlank(self.moduleDict['startwith'])
            multistart = '[startsign:' +self.moduleDict['startwith']+']'

        # 处理命令
        # 'perline': {0: {'startwith': '.*a', 'command': [[{'a': ['A:1']}], [{'b': []}], [{'3': ['B：1']}]]}, 1: {'startwith': '.*2', 'command': [[{'2': []}], [{'3': []}], [{'4': ['C：1']}]]}, 2: {'startwith': '.*6'}}
        commandslist = []
        # try:
        tempcommanddict = self.moduleDict['perline']
        print(tempcommanddict)
        commandstr = ''
        try:
            for linenum, commandline in tempcommanddict.items():  # 循环处理每一行命令
                if commandline=={}:
                    continue
                if 'startwith' in commandline.keys():  # 如果设置了标识才会处理
                    commandstr = '[linesign:' + commandline['startwith']+']'
                    if 'command' not in commandline:
                        commandline['command']=[]
                    commands = commandline['command']
                    for command in commands:
                        for para, positions in command[0].items():  # 处理单行中的参数
                            ##参数中有空格的话提前转换为\s
                            para=self.dealBlank(para)
                            commandstr = commandstr + ' ' + para
                            if positions:  # 设置了参数的话将位置追加到参数后
                                for position in positions:
                                    commandstr = commandstr + ' {' + position + '}'  ##!!!!这里的~应支持自定义，后续需修改
                commandslist.append(commandstr)
        except KeyError as E:
            logging.error(E)
        commandsstr = '\n'.join(commandslist).strip()
        # except TypeError as E:
        #     print(E)
        # 处理结尾
        if self.moduleDict['endwith']:
            self.moduleDict['endwith'] = self.moduleDict['endwith']
            multiend = '[endsign:' +self.moduleDict['endwith']+']'

        modulestr = '\n'.join([multistart, commandsstr, multiend])
        self.modulestrEdit.setText(modulestr.strip())

    def saveFileButton_clicked(self):
        devicename = self.devicetype.text()
        commandtype = self.commandtype.text()
        filestr = 'modules/' + devicename + '/' + commandtype + '.cfg'
        print(filestr)
        if not os.path.exists('modules/' + devicename):
            os.makedirs('modules/' + devicename)
        if not devicename or not commandtype:
            self.showDialog('warning', '请先填入设备类型和待命令类型')
        else:

            print(
                json.dumps(
                    self.moduleDict,
                    separators=(
                        ',',
                        ':'),
                    indent=2,
                    ensure_ascii=False))
            with open(filestr, 'w', encoding='utf-8') as f:
                json.dump(self.moduleDict, f, indent=2, ensure_ascii=False)

    def showDialog(self, dialogtype, warnstr):

        if dialogtype == 'warning':
            QMessageBox.warning(self, '警告', warnstr)
        if dialogtype == 'information':
            QMessageBox.information(self, '信息', warnstr)

    def dealBlank(self,para):
        tempstr=''
        for i in para:
            if i==' ':
                tempstr=tempstr+'\s'
            else:
                tempstr=tempstr+i
        return tempstr

