#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Time : 2022/5/31 22:13
# @Author : rynez
# @File : setdefaultUIDisplay.py
# @Purpose : 高级替换规则生成页面


from PyQt5.QtWidgets import *
from UIDisplay.setdefaultFormUI import *

class toolSetWindow(QWidget, Ui_Form):
    def __init__(self):
        super(toolSetWindow, self).__init__()
        self.setupUi(self)
        self.paradic = {}
        self.signdic = {
            'multiparasplit':'P',
            'marginsign': 'M',
            'spacingsign': 'S',
            'defaultvalue': 'D',
            'replace': 'R',
            'valuecheck': 'V',
            'compare':'C'}
        self.clearButton.clicked.connect(self.clearButton_clicked)
        self.geneButton.clicked.connect(self.geneButton_clicked)
        self.appendButton.clicked.connect(self.appendButton_clicked)
        self.helpButton.clicked.connect(self.helpButton_clicked)

    def open(self):
        self.show()
        self.setFixedSize(self.width(), self.height())

##槽begin##
    def geneButton_clicked(self):
        # 获取各输入框的内容
        self.paradic['elementname'] = self.elementName.text()
        self.paradic['position'] = self.position.text()
        self.paradic['multiparasplit']=self.multiParaSplitEdit.text()
        self.paradic['spacingsign'] = self.spacingSign.text()
        self.paradic['defaultvalue'] = self.defaultValue.text()
        self.paradic['marginsign'] = self.marginSign.text()
        self.paradic['valuecheck'] = self.valueCheck.text()
        self.paradic['compare'] = str(self.compareCheckBox.isChecked())
        self.paradic['replace'] = [
            [self.preValue.text(), self.afterValue.text()]]
        self.printResult()

    def appendButton_clicked(self):
        if not self.preValue.text():
            self.showDialog('information', '请正确输入替换前后的文本')
            return 0
        try:
            self.paradic['replace'].append(
                [self.preValue.text(), self.afterValue.text()])
            self.printResult()
        except KeyError:
            self.showDialog('warning', '请先正确生成规则后再点击追加按钮！')

    def clearButton_clicked(self):
        # 初始化各组件
        self.elementName.setText('')
        self.position.setText('')
        self.spacingSign.setText('')
        self.defaultValue.setText('')
        self.marginSign.setText('')
        self.valueCheck.setText('')
        self.preValue.setText('')
        self.afterValue.setText('')
        self.compareCheckBox.setChecked(False)
        self.paradic = {}

    def helpButton_clicked(self):
        pass

##功能函数begin##
    def printResult(self):
        if self.paradic['elementname']:
            if not self.paradic['position']:
                self.paradic['position'] = '1'
            resultstr = self.paradic['elementname'] + \
                ':' + self.paradic['position']
        else:
            self.showDialog('warning', '必须输入需要处理的参数，如A、B')
            return 0
        for key, value in self.paradic.items():
            # 没有设置的参数则跳过
            if value:
                # 如果是需要替换的元素，则读取一个多重的数组，如果实际内容为空则直接跳过
                if key=='compare' and value=='False':
                    continue
                if key=='spacingsign':
                    if value !='' and not self.compareCheckBox.isChecked():
                        self.showDialog('information','多个参数值不需要合并时指定间隔符号无效')
                        continue
                if key == 'replace':
                    if not self.paradic[key][0][0]:
                        continue
                    templist = []
                    for parm in value:
                        templist.append(':'.join(parm))
                    value = ','.join(templist)
                try:
                    resultstr = resultstr + ';' + \
                        self.signdic[key] + '|' + '\'' + value + '\''
                except KeyError:
                    pass

        resultstr = '{' + resultstr + '}'
        self.rulestr.setText(resultstr)

    def showDialog(self, dialogtype, warnstr):

        if dialogtype == 'warning':
            QMessageBox.warning(self, '警告', warnstr)
        if dialogtype == 'information':
            QMessageBox.information(self, '信息', warnstr)
