#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Time : 2022/4/30 11:18
# @Author : rynez
# @File : converttool.py
import sys
import os
import json
import shlex
import re
import logging
import zlib
import copy
import itertools
from PyQt5 import QtCore, QtGui
from converttoolMainUI import *
from UIDisplay.__init__ import *
# 模板向导页面

class ModuleLackPara(Exception):
  "模板文件缺少内容 "
  def __init__(self,modulestr):
    self.modulestr = modulestr
  def __str__(self):
    return ("模板文件格式不正确:"+self.modulestr)


# 主页面
class toolMainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        # 初始化UI

        self.setupUi(self)
        self.setCommandBox()
        self.premodule = {}
        self.aftermodule = {}
        self.status = self.statusBar()
        self.status.showMessage('ver 0.2.1(preview) Copyright@RyneZ')
        logging.info('主窗口创建成功')
        # 绑定槽
        self.selectConfigFileButton.clicked.connect(
            self.selectConfigFileButton_clicked)
        self.preTampFolderBox.currentIndexChanged.connect(
            self.preTampFolderBox_clicked)
        self.afterTampFolderBox.currentIndexChanged.connect(
            self.afterTampFolderBox_clicked)
        self.preLoadButton.clicked.connect(self.preLoadButton_clicked)
        self.afterLoadButton.clicked.connect(self.afterLoadButton_clicked)
        self.generateButton.clicked.connect(self.generateButton_clicked)
        self.actionsave.triggered.connect(self.actionsave_triggered)
        self.actionload.triggered.connect(self.actionload_triggered)

    def closeEvent(self, event):
        notice = QMessageBox.question(
            self,
            '提示',
            '是否要关闭所有窗口？未保存内容将会丢失。',
            QMessageBox.Yes | QMessageBox.No,
            QMessageBox.No)
        if notice == QMessageBox.Yes:
            event.accept()
            sys.exit(0)
        else:
            event.ignore()

##槽begin##

    def actionsave_triggered(self):
        filename, _ = QFileDialog.getSaveFileName(
            self, '保存文件', '.', '所有文件(*.scfg)')
        try:
            savecontent={}
            savecontent['commands']=self.preContentEdit.toPlainText()
            savecontent['pretamp']=self.preTampEdit.toPlainText()
            savecontent['aftertam']=self.afterTampEdit.toPlainText()
            #print(zlib.compress(str(savecontent).encode('utf-8')))
            outputstr=zlib.compress(str(savecontent).encode('utf-8'))
            with open(filename, 'wb') as f:
                f.write(outputstr)
        except Exception as E:
            logging.critical('unknown error:'+str(E))
            return 0

    def actionload_triggered(self):
        filename, _ = QFileDialog.getOpenFileName(
            self, '打开文件', '.', '所有文件(*.*)')
        try:
            with open(filename, 'rb') as f:
                filelines = f.read()
            try:
                filecontent=zlib.decompress(filelines).decode('utf-8')
            except zlib.error:
                self.showDialog('warning', '非通过本程序保存的配置文件或内容已被篡改，无法导入')
                logging.error(filename+' file contents error')
                return 0
            try:
                if filecontent.startswith('{') and filecontent.endswith('}'):
                    readcontents=eval(filecontent)
                    self.preContentEdit.setText(readcontents['commands'])
                    self.preTampEdit.setText(readcontents['pretamp'])
                    self.afterTampEdit.setText(readcontents['aftertam'])
                else:
                    self.showDialog('warning', '文件内容非本程序导出数据，无法导入')
                    logging.critical('filecontent:'+filelines.decode())
            except KeyError:
                self.showDialog('warning','该文件缺失内容，无法导入')
                logging.error(filename+' filecontent:'+filelines.decode())
        except FileNotFoundError:
            return 0
        except Exception as E:
            logging.critical('unknown error:' + E)
            return 0

    def selectConfigFileButton_clicked(self):
        filename, _ = QFileDialog.getOpenFileName(
            self, '打开文件', '.', '所有文件(*.*)')
        if filename:
            self.importFileNameEdit.setText(filename)
            try:
                with open(filename, 'r', encoding='utf-8') as f:
                    filelines = f.read()
            except FileNotFoundError:
                self.showDialog('warning', '文件不存在，请检查目录或者权限设置是否正常')
            except PermissionError:
                self.showDialog('warning','未选中模板或者目录权限存在问题')
            self.preContentEdit.setText(filelines)

    def preTampFolderBox_clicked(self):
        print(self.preTampFolderBox.currentText())
        self.preTampTypeBox.clear()
        totalfiles = []
        for root, dirs, files in os.walk(
                './modules/' + self.preTampFolderBox.currentText()):
            totalfiles = files
            break
        self.preTampTypeBox.addItems(totalfiles)

    def afterTampFolderBox_clicked(self):
        print(self.afterTampFolderBox.currentText())
        self.afterTampTypeBox.clear()
        totalfiles = []
        for root, dirs, files in os.walk(
                './modules/' + self.afterTampFolderBox.currentText()):
            totalfiles = files
            break
        self.afterTampTypeBox.addItems(totalfiles)

    def preLoadButton_clicked(self):
        #self.preTampEdit.setText('./modules/' +self.preTampFolderBox.currentText()+'/'+self.preTampTypeBox.currentText())
        try:
            filepath = './modules/' + self.preTampFolderBox.currentText() + '/' + \
                self.preTampTypeBox.currentText()
            with open(filepath, 'r', encoding='utf-8') as f:
                filecontent = json.load(f)
            setstr = self.dictToModule(filecontent)
            self.preTampEdit.setText(setstr)
            self.moduleToDict(setstr)   #调试用
        except json.decoder.JSONDecodeError:
            self.showDialog('warning', '模板文件内容存在问题，请检查文件或者重新生成')
        except FileNotFoundError:
            self.showDialog('warning', '文件不存在，请检查目录或者权限设置是否正常')
        except PermissionError:
            self.showDialog('warning','未选中模板或者目录权限存在问题')

    def afterLoadButton_clicked(self):
        #self.afterTampEdit.setText('./modules/' +self.afterTampFolderBox.currentText()+'/'+self.afterTampTypeBox.currentText())
        try:
            filepath = './modules/' + self.afterTampFolderBox.currentText() + '/' + \
                self.afterTampTypeBox.currentText()
            with open(filepath, 'r', encoding='utf-8') as f:
                filecontent = json.load(f)
            setstr = self.dictToModule(filecontent)
            self.afterTampEdit.setText(setstr)
        except json.decoder.JSONDecodeError:
            self.showDialog('warning', '模板文件内容存在问题，请检查文件或者重新生成')
        except FileNotFoundError:
            self.showDialog('warning', '文件不存在，请检查目录或者权限设置是否正常')
        except PermissionError:
            self.showDialog('warning','未选中模板或者目录权限存在问题')

    def generateButton_clicked(self):
        startreg=''
        endreg=''
        perlinereg=''

        if not self.preContentEdit.toPlainText():
            self.showDialog('warning', '待转换内容未添加')
            return 0
        if not (self.preTampEdit.toPlainText()
                and self.afterTampEdit.toPlainText()):
            self.showDialog('warning', '转换前后模板未全部添加')
            return 0
        commandcontents = self.preContentEdit.toPlainText()
        premodulestr = self.preTampEdit.toPlainText().strip()
        aftermodulestr = self.afterTampEdit.toPlainText().strip()
        try:
            premoduledict=self.moduleToDict(premodulestr)
            aftermoduledict=self.moduleToDict(aftermodulestr)
        except ModuleLackPara as E:
            self.showDialog('warning','模板内容中替换规则格式不正确，缺少参数定位信息')
            return 0
        #示例dict={{'perline': {0: {'linesign': '.*set\\sservice', 'command': ['set', 'service', '"ips\\sshuju-yinqin"', 'protocol', 'tcp', 'src-port', '0-65535', 'dst-port', '{B-C:1}', '4433-4434']}, 1: {'linesign': '.*set\\sservice.*\\sudp', 'command': ['set', 'service', '"ips\\sshuju-yinqin"', '+', 'udp', 'src-port', '0-65535', 'dst-port', '{A:1}', '65432-65432']}}, 'startsign': '.*set\\sser\\svice\\s"ips\\sshuju-yinqin"\\sprotocol', 'endsign': '.*set\\sservice\\s"ips\\sshuju-yinqin"'}
        #作为输入的模板将有4种可能性：有startwith、perline和endwith；只有startwith，此情况下将匹配到下次startwith；只有perline，将只单行来匹配；有问题的模板：除了前三种都是
        #print(premoduledict)
        #开始处理待转换内容的模板
        #print(premoduledict)
        #使用dealCommands函数将输入的文本内容按照模板分成多段，格式：{0:['','',...],}

        perblockcommandsdict,matchedcommandsblockdict=self.dealCommands(commandcontents,premoduledict)
        #print(commandsblockdict)
        #使用getParaValue函数将commandsblockdict中的命令通过premoduledict中的paras读取出来,再通过geneNewCommand生成新的命令
        newcommands={}
        self.resultBrowser.append('根据待转换模板共匹配出'+str(len(matchedcommandsblockdict))+'个命令block')
        for blockcount, percommandsblock in matchedcommandsblockdict.items():
            self.resultBrowser.append('处理block:'+str(blockcount))
            #1. 先根据取值
            paravalues=self.getParaValue(percommandsblock,premoduledict['perline'])
            #print(paravalues)
            #2. 根据高级规则对返回的值进行处理
            dealedparavalues=self.dealParas(paravalues)
            #3. 根据替换后模板生成新的命令
            newcommand=self.geneNewCommands(dealedparavalues,aftermoduledict)
            newcommands[blockcount]=newcommand
        compareWindow.open(perblockcommandsdict,matchedcommandsblockdict,newcommands)

##功能函数begin##

    def showDialog(self, dialogtype, warnstr):

        if dialogtype == 'warning':
            QMessageBox.warning(self, '警告', warnstr)
        if dialogtype == 'information':
            QMessageBox.information(self, '信息', warnstr)
    # 初始化下拉列表1，目前仅获取目录名

    def setCommandBox(self):
        totaldirs = ['',]
        for root, dirs, files in os.walk('./modules'):
            totaldirs = totaldirs+dirs
            break
            # print('root_dir:', root)  # 当前路径
            # print('sub_dirs:', dirs)  # 子文件夹
            # print('files:', files)  # 文件名称，返回list类型
        # for dir in totaldirs:
        self.preTampFolderBox.addItems(totaldirs)
        self.afterTampFolderBox.addItems(totaldirs)
        #print(totaldirs)

    #从json获取内容转换为模板字符串
    def dictToModule(self, paradict):
        #print(paradict)
        multistart = ''
        multiend = ''
        try:
            if paradict['startwith']:
                paradict['startwith']=self.dealBlank(paradict['startwith'])
                multistart = '[startsign:' + paradict['startwith']+']'
        except KeyError:
            self.showDialog('warining', '模板文件中缺少[startwith]部分，请检查文件内容是否正确。')
            logging.warning('\'startsign\' not in ' + str(paradict))
            # 处理命令
        commandslist = []
        try:
            tempcommanddict = paradict['perline']
        except KeyError:
            self.showDialog('warining', '模板文件中缺少[perline]部分，请检查文件内容是否正确。')
            logging.warning('\'perline\' not in ' + str(paradict))
        commandstr = ''
        for linenum, commandline in tempcommanddict.items():  # 循环处理每一行命令
            if 'startwith' in commandline.keys():  # 如果设置了标识才会处理

                commandstr = '[linesign:' + commandline['startwith']+']'
                commands = commandline['command']
                for command in commands:
                    for para, positions in command[0].items():  # 处理单行中的参数
                        ##提前处理参数中空格
                        para=self.dealBlank(para)
                        commandstr = commandstr + ' ' + para
                        if positions:  # 设置了参数的话将位置追加到参数后
                            for position in positions:
                                commandstr = commandstr + ' {' + position + '}'
            else:
                continue
            commandslist.append(commandstr)

        commandsstr = '\n'.join(commandslist).strip()
        # 处理结尾
        try:
            if paradict['endwith']:
                paradict['endwith']=self.dealBlank(paradict['endwith'])
                multiend = '[endsign:' + paradict['endwith']+']'
        except KeyError:
            self.showDialog('warining', '模板文件中缺少[endsign]部分，请检查文件内容是否正确。')
            logging.warning('\'endsign\' not in ' + str(paradict))
        modulestr = '\n'.join([multistart, commandsstr, multiend])
        return modulestr.strip()

    def moduleToDict(self,modulestr):
        #print(modulestr)
        moduledict={'perline':{}}
        perlinenum = 0
        for line in modulestr.splitlines():
            line = line.strip()
            linetemp=shlex.shlex(line,punctuation_chars=True)
            #linetemp.whitespace=' '
            linetemp.commenters=''
            linetemp.quotes='~"'
            linetemp.whitespace_split=True
            linelist=list(linetemp)
            #print(linelist)
            if len(linelist)==0:
                continue
            if type(linelist[0])==list:
                sign=' '.join(linelist[0])
            else:
                sign=linelist[0]
            #print(linelist[1])
            signreg=self.dealSign(sign) #获取匹配起始字符串的正则表达式
            if not signreg:
                continue
            if sign.startswith('[startsign:'):
                moduledict['startsign']=signreg
            elif sign.startswith('[linesign:'):
                moduledict['perline'][perlinenum]={}
                moduledict['perline'][perlinenum]['linesign']=signreg
                moduledict['perline'][perlinenum]['command']=linelist[1:]
                moduledict['perline'][perlinenum]['paras']=self.getParas(linelist[1:])
                perlinenum=perlinenum+1
            elif sign.startswith('[endsign:'):
                moduledict['endsign']=signreg

        return(moduledict)

    def dealBlank(self,para):
        #将字符串中的空格转换为\s，一是为了避免对二次进行分割字符串造成影响，二是可以直接用来进行正则匹配
        tempstr=''
        for i in para:
            if i==' ':
                tempstr=tempstr+'\s'
            else:
                tempstr=tempstr+i
        return tempstr

    def dealSign(self,signstr):
        #将[xxxsign:***]中的匹配字符串取出
        signreg=re.compile('^\[(start|line|end)sign:(.*)\]$')
        signmatch=signreg.match(signstr)
        #print(signmatch.group(2))
        if signmatch:
            return(signmatch.group(2))

    def dealCommands(self,commandcontents,premoduledict):
            if premoduledict['perline']=={}:
                self.showDialog('warining','待转换模板内容中缺少[perline]部分，请检查格式是否正确。')
                logging.warning('\'perline\' not in '+str(premoduledict))
                return 0
        # try:
            commandsblockdict = {}
            perblockcommandsdict={}
            blockcount = 0  # 每一行/多行符合模板的代码属于一个block(块)
            #条件1：有startsign标识，但不一定有endsign
            if 'startsign' in premoduledict.keys():
                startreg=re.compile(premoduledict['startsign'])
                if 'endsign' in premoduledict.keys():
                    endreg=re.compile(premoduledict['endsign'])
                else:
                    endreg=re.compile(premoduledict['startsign'])
                isnewblock=0 #判断是否属于同一个块
                issameline=0 #如果是同一行，则匹配完每代码块的开始和每一行标志后不直接匹配end,要到第二行开始再匹配结束的标志
                isinitialize = 0
                for line in commandcontents.splitlines():
                    # print(isnewblock,issameline)
                    issameline2 = 0  # 如果是同一行，匹配到一次perline后不再匹配下一次
                    startmatch=startreg.match(line)
                    if (not issameline) and startmatch: #匹配到结束标志后issameline会变为0，此时再开始匹配开始标识，节省系统资源
                        # print(startmatch.group(0))
                        # print(blockcount)
                        if not isinitialize:
                            commandsblockdict[blockcount] = {}
                            perblockcommandsdict[blockcount] = []
                            for i in range(len(premoduledict['perline'])):
                                commandsblockdict[blockcount][i] = []
                            isinitialize = 1
                        isnewblock = 1
                        issameline=0
                    if isnewblock:
                        # 用以判断每条匹配到的命令分别是模板里的哪一条匹配出来的，只会执行1次
                        currentline=0
                        for linenum,commanddict in premoduledict['perline'].items():
                            perblockcommandsdict[blockcount].append(line)
                            # if commandlist['linesign']==None:
                            #     continue
                            #print(commanddict)
                            if not issameline2:
                                perlinereg=re.compile(commanddict['linesign'])
                                perlinematch=perlinereg.match(line)
                                if perlinematch:
                                    commandsblockdict[blockcount][currentline].append(line)
                                    issameline2=1
                            currentline=currentline+1
                    if issameline and isnewblock:
                        isnewblock=0
                        issameline=-1
                        endmatch=endreg.match(line)
                        if endmatch:
                            #commandsblockdict[blockcount].append(line)
                            blockcount=blockcount+1
                            isinitialize=0
                    issameline = issameline + 1

            #条件2：无startsign，只有endsign和perline
            elif 'endsign' in premoduledict.keys():
                endreg=re.compile(premoduledict['endsign'])
                isinitialize = 0
                for line in commandcontents.splitlines():
                    issameline=0
                    currentline = 0
                    if not isinitialize:
                        commandsblockdict[blockcount] = {}
                        perblockcommandsdict[blockcount] = []
                        for i in range(len(premoduledict['perline'].items())):
                            commandsblockdict[blockcount][i] = []
                        isinitialize = 1
                    #print(commandsblockdict[blockcount])
                    for linenum, commanddict in premoduledict['perline'].items():
                        #一行只匹配一条perline
                        if not issameline:
                            perblockcommandsdict[blockcount].append(line)
                            perlinereg = re.compile(commanddict['linesign'])
                            perlinematch = perlinereg.match(line)
                            if perlinematch:
                                #print(blockcount,currentline)
                                commandsblockdict[blockcount][currentline].append(line)
                                issameline=1
                        currentline=currentline+1
                    endmatch = endreg.match(line)
                    if endmatch:
                        blockcount = blockcount + 1
                        isinitialize = 0

            #条件3：只有perline，即单行，或者连续(!)的多行
            else:
                isinitialize = 0
                matched=0
                for line in commandcontents.splitlines():
                    currentline = 0
                    if not isinitialize:
                        commandsblockdict[blockcount] = {}
                        perblockcommandsdict[blockcount] = []
                        for i in range(len(premoduledict['perline'].items())):
                            commandsblockdict[blockcount][i] = []
                        isinitialize = 1
                    for linenum, commanddict in premoduledict['perline'].items():
                        perblockcommandsdict[blockcount].append(line)
                        perlinereg = re.compile(commanddict['linesign'])
                        perlinematch = perlinereg.match(line)
                        if perlinematch:

                            #print(perlinematch.group(),blockcount,currentline)
                            commandsblockdict[blockcount][currentline].append(line)
                            blockcount = blockcount + 1
                            isinitialize = 0
                            matched=1
                            break
                        currentline = currentline + 1  # 预留参数，暂时无用，连续匹配多行时会用到
                    #如果匹配到linesign但没有匹配到具体参数，则放弃该行，考虑到有默认值的设定，此部分后续可根据参数自行决定是否启用
                    # if not matched:
                    #     commandsblockdict.pop(blockcount)
                    #     isinitialize = 0
                    #     matched=0



        # except Exception as e:
        #     print(e)
            #logging.error('unknown error ooccurred:'+e)
            return perblockcommandsdict,commandsblockdict

    def getParas(self,commandlist):
        #eg:['set', 'service', '"ips\\sshuju-yinqin"', 'protocol', 'tcp', 'src-port', '0-65535', 'dst-port', '{B-C:1}', '4433-4434']
        #print(commandlist)
        parareg=re.compile('\{(.*:.*)\}')
        count=0
        matched=-100
        resultdict={}
        linenum=0   #将参数与line的行数挂钩
        for parastr in commandlist:
        #先按照参数来读取
        #eg:{'src-port': ["A:2,S|',',D|'default',M|''',C|'*.*',R|'permit:allow,permit2:allow2'", 'A:1'], 'dst-port': ['C:1']}
            paramatch=parareg.match(parastr)
            if paramatch:
                #paranum=len(paramatch)
                if matched+1!=count:
                    paraname=commandlist[count - 1]

                parastr=paramatch.group(1)
                para = parastr.strip()
                paratemp = shlex.shlex(para, punctuation_chars=True)
                paratemp.whitespace=';'
                paratemp.commenters = ''
                paratemp.quotes = ''
                paratemp.whitespace_split = True
                paralist = list(paratemp)
                #print(paralist)
                #判断是否为正常的替换模板，如果切分后的列表中第一个元素不符合规则则跳过
                signreg=re.compile('.*:\d+$')
                #print(paralist[0])
                if signreg.match(paralist[0]):
                    parastart=paralist[0].split(':')
                    if parastart[0] not in resultdict.keys():
                        resultdict[parastart[0]]=[]
                    paralist[0]=':'.join([paraname,parastart[1]])

                    resultdict[parastart[0]].append(paralist)

                else:
                    logging.error('替换规则不符合要求：'+parastr)
                    raise ModuleLackPara(parastr)
                matched = count
            count=count+1
        #print(resultdict)
        #进一步按照参数值来处理
        return resultdict

    def getParaValue(self,commandsblockdict,moduledict):
        #循环遍历每一个命令/block,先从输入的命令中根据值的位置进行取值
        # for blockcount, percommandsblock in commandsblockdict.items():
        #print(moduledict)
        valuedict = {}
        # 遍历按模板不同perline匹配出来的命令
        for moduleseq,commands in commandsblockdict.items():
            if commands:
                #遍历每一条命令
                for percommand in commands:
                    command = percommand.strip()
                    commandtemp = shlex.shlex(command, punctuation_chars=True)
                    # linetemp.whitespace=' '
                    commandtemp.commenters = ''
                    commandtemp.quotes = '~"'
                    commandtemp.whitespace_split = True
                    commandlist = list(commandtemp)
                    #遍历模板中每一个参数，看是否有对应的值
                    for paraname,parasdetail in  moduledict[moduleseq]['paras'].items():
                        #print(parasdetail)
                        if paraname not in valuedict.keys():
                            valuedict[paraname]=[]
                        for paradetail in parasdetail:
                            (opponame,position)=paradetail[0].split(':')
                            try:
                                paraoppolocation=commandlist.index(opponame)
                                valueposition=(paraoppolocation+int(position))
                                valuedict[paraname].append([moduleseq,paradetail,commandlist[valueposition]])
                            except ValueError:
                            #如果没有在命令中找到参数，(通过正则进行匹配)，如果确实没有则记录日志并匹配下一个
                                logging.info('未在命令中找到参数:'+str(opponame))
        #print(valuedict)
        return (valuedict)

    def dealParas(self,valuesdict):
        #{'A': [[1, ['src-port:2', "S|','", "D|'default'", "M|'''", "C|'*.*", ",R|'permit:allow,permit2:allow2'"], 'dst-port']], 'B': [[1, ['src-port:1'], '0-65530'], [1, ['dst-port:1'], '65432-65435']]}
        signdic = {
            'multiparasplit': 'P',
            'replace': 'R',
            'valuecheck': 'V',
            }
        #newvalues=dict.fromkeys(valuesdict.keys())
        newvalues={}    #存放处理后的参数值
        for paraname,values in valuesdict.items():
            #[[1, ['src-port:1'], '0-65530'], [1, ['dst-port:1'], '65432-65435']],0为第几行模板
            for value in values:
                #print(value):[[1, ['src-port:1'], '0-65530'], [1, ['dst-port:1'], '65432-65435']],0为第几行模板
                seniorsettings=value[1][1:]   #符合某个参数对应的高级参数列表
                # 如果有高级替换规则，则先处理完规则后再处理参数值
                if seniorsettings:
                    #将高级规则转换为字典形式，eg:{'C': "'*.*'", 'R': "'permit:allow,permit2:allow2'"}
                    settingsdict={}
                    for seniorsetting in seniorsettings:
                        for sign in signdic.values():
                            if seniorsetting.startswith(sign):
                                settingreg=re.compile('([A-Z])\|(.*)')
                                settingmatch=settingreg.match(seniorsetting)
                                settingsdict[settingmatch.group(1)]=settingmatch.group(2)
                    #print(settingsdict)
                    #开始循环匹配不同的高级规则处理参数
                    # for settingname,settingvalue in settingsdict.items():
                    #
                    paralist=paraname.split(',')
                    # 如果一个参数值是多个参数对应值的集合，例如需要将‘1000-2000’中1000作为参数A，2000作为参数B
                    if 'P' in settingsdict.keys():
                        valuelist=value[2].split(settingsdict['P'][1:-1])
                    else:
                        valuelist=[value[2]]
                    if len(paralist)!=len(valuelist):
                        self.showDialog('warining','参数:'+paraname+'与使用'+settingsdict['P']+'分割后的参数值数量不匹配')
                        logging.error('参数:'+paraname+'与使用'+settingsdict['P']+'分割后的参数值'+value[2]+'数量不匹配')
                    for num in range(len(paralist)):
                        if paralist[num] not in newvalues.keys():
                            newvalues[paralist[num]]=[]
                        #如果有需要校验的则先根据使用者提供的正则进行校验，如果不满足则跳过
                        if 'V' in settingsdict.keys():
                            checkreg=re.compile(settingsdict['V'][1:-1])
                            checkresult=checkreg.match(valuelist[num])
                            # if checkresult:
                            #     print(value[2]+'符合'+settingsdict['V'])
                            # else:
                            #     print('value检查未通过')
                            #     continue
                            if not checkresult:
                                continue
                        #如果有需要替换的内容则先进行替换
                        if 'R' in settingsdict.keys():
                            replaces=settingsdict['R'][1:-1]
                            for replace in replaces.split(','):
                                (prestr,afterstr)=replace.split(':')
                                if valuelist[num]==prestr:
                                    valuelist[num]=afterstr
                        newvalues[paralist[num]].append(valuelist[num])
                else:
                    if paraname not in newvalues.keys():
                        newvalues[paraname]=[]
                    newvalues[paraname].append(value[2])
                    #print(paraname+'有一个取值是：'+value[2]+'，该值需要使用'+' '.join(value[1][1:])+'处理')
        return newvalues

    def geneNewCommands(self,paravaluedict,aftermoduledict):
        aftermoduledict=copy.deepcopy(aftermoduledict)
        paravaluedict=copy.deepcopy(paravaluedict)
        signdic = {
            'marginsign': 'M',
            'spacingsign': 'S',
            'defaultvalue': 'D',
            'compare': 'C'}
        #print(paravaluedict)
        newcommandslist=[]
        if aftermoduledict['perline'] == {}:
            self.showDialog('warining', '待转换模板内容中缺少[perline]部分，请检查格式是否正确。')
            logging.warning('\'perline\' not in ' + str(aftermoduledict))
            return 0
        else:
            for num,perlinedict in aftermoduledict['perline'].items():

                for command in perlinedict['command'][::-1]:
                    # 如果是替换规则则从列表中移除
                    parareg = re.compile('\{(.*:.*)\}')
                    isparastr = parareg.match(command)
                    if isparastr:
                        perlinedict['command'].remove(command)
                #print(perlinedict['command'])
                parasdict=copy.deepcopy(perlinedict['paras'])
                #print(perlinedict['command'])
                #循环遍历命令拆分后的列表，找出需要替换的参数值并按照替换规则填入parasdict的参数值
                for paraname,paralist in parasdict.items():
                    signreg = re.compile('.*:\d+$')
                    # 先根据paraname将参数值取出，然后通过高级替换规则生成最终的内容,如果一行有多个相同的参数，目前只取第一个
                    settingsdict = {}#存放当前paralist中有哪些高级参数
                    if signreg.match(paralist[0][0]):
                        #paralist[0]=['add:1']
                        parastart = paralist[0][0].split(':')
                        commandstr=parastart[0]
                        commandposition=parastart[1]
                        #如果没有高级参数，则直接将paravaluedict[paraname]放到commandstr对应的偏移commandposition处
                        seniorsettings = paralist[0][1:]  # 符合某个参数对应的高级参数列表
                        # 如果有高级替换规则，则先处理完规则后再处理参数值
                        #print(paraname,seniorsettings)

                        if seniorsettings:
                            # 将高级规则转换为字典形式，eg:{'C': "'*.*'", 'R': "'permit:allow,permit2:allow2'"}
                            for seniorsetting in seniorsettings:
                                for sign in signdic.values():
                                    if seniorsetting.startswith(sign):
                                        settingreg = re.compile('([A-Z])\|(.*)')
                                        settingmatch = settingreg.match(seniorsetting)
                                        settingsdict[settingmatch.group(1)] = settingmatch.group(2)

                            #print(settingsdict)
                    #处理不同的
                    #print(perlinedict['command'])
                    for command in perlinedict['command']:

                        if command == commandstr:
                            #print(command,perlinedict['command'].index(command))
                            newposition=perlinedict['command'].index(command)+int(commandposition)
                            try:
                                paravalue=paravaluedict[paraname]
                            except KeyError:
                                #判断是否存在默认值
                                if 'D' in settingsdict.keys():
                                    defaultvalue = settingsdict['D'][1:-1]
                                    paravalue=[defaultvalue]
                                else:
                                    continue
                            #需考虑生成多行
                            if 'S' in settingsdict.keys():
                                spacingsign=settingsdict['S'][1:-1]
                            else:
                                spacingsign=' '

                            if 'C' in settingsdict.keys():
                                if 'M' in settingsdict.keys():
                                    insertvalue=(settingsdict['M'][1:-1]+spacingsign.join(paravalue)+settingsdict['M'][1:-1])
                                    perlinedict['command'].insert(newposition, insertvalue)
                                else:
                                    perlinedict['command'].insert(newposition, spacingsign.join(paravalue))
                            else:
                                #需要重复生成多行是如果列表存在不是str的元素会出现typeerror，以此来简化处理
                                if 'M' in settingsdict.keys():
                                    for i in range(len(paravalue)):
                                        paravalue[i]=settingsdict['M'][1:-1]+paravalue[i]+settingsdict['M'][1:-1]
                                perlinedict['command'].insert(newposition, paravalue)
                try:
                    newcommandstr=' '.join(perlinedict['command'])
                    newcommandslist.append(newcommandstr)
                except TypeError:
                    #出现TypeError代表单个参数对应的值有多个，且需要生成多行命令
                    #print(perlinedict['command'])
                    tempcommandlist=copy.deepcopy(perlinedict['command'])
                    newcommanddict={}
                    #newcommandlist=[]
                    num=0
                    islist=0
                    for tempcommand in tempcommandlist:
                        if type(tempcommand)==list:
                            islist=1
                        if islist==1:
                            newcommanddict[num]=tempcommand
                            islist=0
                        num = num + 1
                    #如果有多个多参数值的通过笛卡尔乘积生成
                    newparaslist=[]
                    if len(newcommanddict)>1:
                        for newcommand in itertools.product(*newcommanddict.values()):
                            newparaslist.append(newcommand)
                        for i in range(len(newparaslist)):
                            paraposition=newcommanddict.keys()
                            for j in range(len(paraposition)):
                                tempcommandlist[list(paraposition)[j]]=newparaslist[i][j]
                            newcommandstr = ' '.join(tempcommandlist)
                            newcommandslist.append(newcommandstr)
                    else:

                        for position,value in newcommanddict.items():
                            for i in range(len(value)):
                                tempcommandlist[position]=value[i]
                                newcommandstr = ' '.join(tempcommandlist)
                                newcommandslist.append(newcommandstr)
            return newcommandslist


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, filename='log', format="%(asctime)s %(levelname)s：%(funcName)s：%(message)s")
    # 适应高DPI设备
    QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling)
    # 适应Windows缩放
    QtGui.QGuiApplication.setAttribute(QtCore.Qt.HighDpiScaleFactorRoundingPolicy.PassThrough)
    app = QApplication(sys.argv)

    mainWindow = toolMainWindow()
    # ui=nettoolsMainUI.Ui_MainWindow()
    # ui.setupUi(mainWindow)
    setWindow = toolSetWindow()
    tampWindow = tampSetWindow()
    compareWindow= resultCompareWindow()
    mainWindow.show()
    mainWindow.geneStrButton.clicked.connect(setWindow.open)
    mainWindow.actiongenerateTamp.triggered.connect(tampWindow.open)

    sys.exit(app.exec())
