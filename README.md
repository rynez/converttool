# 配置转换/生成工具

​    很多网络工程师会遇到不同厂商间设备替换的工作，比如Juniper、Cisco、华为等，但往往设备之间命令的差异会导致工程师花费很长时间在转换配置上面，本工具尽可能在保持一定通用性的同时简化生成配置的过程，按照格式提供模板数据后可以自动生成其他格式的配置文件。后续也会添加更多语言的支持。
    Many network engineers are faced the replacement of equipment from different factory,such as Juniper,Cisco,HuaWei and other factory,the difference of commands between devices will cause engineers to spend a long time on converting configurations
.This tool simplifies the process of generating configuration while maintaining certain generality as far as possible,After providing template data according to format, configuration files in other formats can be automatically generated.More language support will be added soon.
## 程序如何工作

​    

## 支持的格式
预处理的时候只标出哪些是参数名
{A:1;D|permit;R|no:deny,yes:permit;C|',';P|'''}

参数放在替换前和替换后的命令中作用是相反的

## 更新计划
1. 对比颜色自定义
2. 如果没有指定块起始和结尾标志时，支持匹配多行连续的值()
3. 自定义命令的分割符号，目前均默认为空格